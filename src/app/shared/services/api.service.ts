import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {catchError, retry} from "rxjs/operators";
import {Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // Tento druh service by měl být generovaný ze Swageru společně s datovým interface - pro DEMO účely je psaná ručně
  constructor(private readonly http: HttpClient) {
  }

  public getHomepageData(): Observable<any> {
    return this.http.get<any>('/api/homepage')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  private handleError(error: any): Observable<never> {
    let errorMessage = '';

    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error?.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
