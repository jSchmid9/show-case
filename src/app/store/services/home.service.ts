import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ApiService} from "../../shared/services/api.service";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private readonly apiService: ApiService) {
  }

  public getHomePageData(): Observable<any> {
    return this.apiService.getHomepageData()
      .pipe(map(response => response));
  }
}
