import {Injectable} from '@angular/core';
import {HomePageDataInterface} from "../model/home-page-data.interface";

@Injectable({
  providedIn: 'root'
})
export class HomePageDataService {
  private usedNumbers: Set<number> = new Set<number>();

  private saveNewRandomNumber(newRandomNumber: number, permanentSource: Set<number>, temporarySource: Set<number>): void {
    permanentSource.add(newRandomNumber);
    temporarySource.add(newRandomNumber);
  }

  private generateRandomInteger(range: number | undefined = 0): number {
    return Math.floor(Math.random() * range);
  }

  private deleteFirstFourIntegers(): number[] {
    return [...this.usedNumbers].slice(4, 8);
  }

  private getNewRandomItems(state: HomePageDataInterface): any[] {
    const zeroItems: number = 0;
    const newNumbers: Set<number> = new Set<number>();
    let newRandomItems: any[] = [];

    if (!state.crazyPricesItems || state.crazyPricesItems?.length === zeroItems)
      return [];

    while (newNumbers?.size < 4) {
      const newRandomNumber: number = this.generateRandomInteger(state.crazyPricesItems?.length)

      if (!newNumbers.has(newRandomNumber) && !this.usedNumbers.has(newRandomNumber)) {
        this.saveNewRandomNumber(newRandomNumber, newNumbers, this.usedNumbers)
        newRandomItems.push(state.crazyPricesItems[newRandomNumber]);
      }
    }

    if (this.usedNumbers?.size > 4) {
      this.usedNumbers = new Set(this.deleteFirstFourIntegers());
    }

    return newRandomItems;
  }

  private setCrazyPricesItems(source: any): object[] {
    const CRAZY_PRICES_ITEMS = 'CrazyPricesItems';
    let allCrazyPricesItems: object[] = [];

    source?.introPart.forEach((arr: any) => {
      const crazyPricesItems: Object[] = arr.filter((item: any) => item?.type === CRAZY_PRICES_ITEMS);
      crazyPricesItems.forEach((it: any) => it?.content?.items.forEach((item: any) => allCrazyPricesItems.push(item)));
    });

    return allCrazyPricesItems;
  }

  public transformResponse(response: any): HomePageDataInterface {
    return {
      crazyPricesItems: this.setCrazyPricesItems(response)
    };
  }

  public setRandomCrazyPricesItems(state: HomePageDataInterface): any[] {
    return this.getNewRandomItems(state);
  }
}
