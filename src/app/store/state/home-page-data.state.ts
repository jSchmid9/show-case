import {Action, Selector, State, StateContext, StateToken} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {HomeService} from '../services/home.service';
import {HomePageDataService} from '../services/home-page-data.service';
import {HomePageDataInterface} from '../model/home-page-data.interface';
import {GetHomePageData} from "../actions/home-page-data.action";
import {GetRandomCrazyPricesItems} from "../actions/random-crazy-items.action";

export const HOME_DATA_STATE_TOKEN = new StateToken<string>('HomePageDataModel');

@State<HomePageDataInterface>({
  name: HOME_DATA_STATE_TOKEN,
  defaults: {
    crazyPricesItems: [],
    randomCrazyPricesItems: []
  }
})

@Injectable()
export class HomePageDataState {
  constructor(private readonly homeService: HomeService,
              private readonly homeScreenDataService: HomePageDataService) {
  }

  @Selector()
  public static randomCrazyPricesItems(state: HomePageDataInterface): any[] {
    return state.randomCrazyPricesItems;
  }

  @Selector()
  public static crazyPricesItems(state: HomePageDataInterface): any[] {
    return state.crazyPricesItems;
  }

  @Action(GetHomePageData)
  public getHomeScreenDataForUser(ctx: StateContext<HomePageDataInterface>): void {
    this.homeService.getHomePageData().pipe(
      tap((result) => {
        ctx.patchState(this.homeScreenDataService.transformResponse(result));
        ctx.dispatch(new GetRandomCrazyPricesItems());
      })).subscribe();
  }

  @Action(GetRandomCrazyPricesItems)
  public getRandomCrazyPricesItems(ctx: StateContext<HomePageDataInterface>): void {
    ctx.patchState({
      randomCrazyPricesItems: this.homeScreenDataService.setRandomCrazyPricesItems(ctx.getState())
    });
  }
}
