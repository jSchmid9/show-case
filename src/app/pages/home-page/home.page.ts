import {Component, ElementRef, ViewChild} from '@angular/core';
import {Observable} from "rxjs";
import {Select, Store} from "@ngxs/store";
import {GetHomePageData} from "../../store/actions/home-page-data.action";
import {HomePageDataState} from "../../store/state/home-page-data.state";
import {GetRandomCrazyPricesItems} from "../../store/actions/random-crazy-items.action";

@Component({
  selector: 'app-home.page',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage {
  @ViewChild('checkboxTg') checkboxTg: ElementRef;
  private readonly GRAYSCALE_OFF = 'grayscale-off';
  private readonly GRAYSCALE_ON = 'grayscale-on';
  public isToggleSwitchEnabled: string = this.GRAYSCALE_OFF;

  @Select(HomePageDataState.randomCrazyPricesItems)
  randomCrazyPricesItems$: Observable<any[]>;

  constructor(private readonly store: Store) {
    this.store.dispatch(new GetHomePageData());
  }

  public showNewRandomItems(): void {
    this.store.dispatch(new GetRandomCrazyPricesItems());
  }

  public onToggleSwitch(): void {
    this.isToggleSwitchEnabled = this.getGrayscaleCssClass(this.checkboxTg.nativeElement.checked);
  }

  private getGrayscaleCssClass(isChecked: boolean): string {
    return isChecked ? this.GRAYSCALE_ON : this.GRAYSCALE_OFF;
  }
}
