import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Observable} from "rxjs";

@Component({
  selector: 'tile-component',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TileComponent {
  @Input() public randomCrazyPricesItems: Observable<any[]>;
  @Input() public grayscaleCssClass: string;

  public trackById(index: number, item: any): number {
    return item?.id;
  }

}
