import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HomePage} from './Home.page';
import {NgxsModule} from "@ngxs/store";
import {TranslateModule} from "@ngx-translate/core";

describe('HomePage.PageComponent', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [NgxsModule.forRoot(), TranslateModule.forRoot()]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
