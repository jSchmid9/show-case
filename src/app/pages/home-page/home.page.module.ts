import {NgModule} from '@angular/core';
import {TranslateModule} from "@ngx-translate/core";
import {HomePage} from "./home.page";
import {TileComponent} from "./components/tile/tile.component";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  providers: [],
  declarations: [
    HomePage,
    TileComponent
  ]
})

export class HomePageModule {
}
